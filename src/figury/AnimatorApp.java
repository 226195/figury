package figury;

import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.JButton;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import javax.swing.*;
import javax.swing.event.*;

public class AnimatorApp extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					final AnimatorApp frame = new AnimatorApp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param delay 
	 */
	public AnimatorApp() {
		final int inc = 25;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		int ww = 450, wh = 300;
		setBounds((screen.width-ww)/2, (screen.height-wh)/2, ww, wh);
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);

		AnimPanel kanwa = new AnimPanel();
		kanwa.setBounds(10, 11, (ww-28), (wh-71));
		contentPane.add(kanwa);
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				kanwa.initialize();
			}
		});

		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				kanwa.addFig();
			}
		});
		btnAdd.setBounds(10, 239, 80, 23);
		contentPane.add(btnAdd);
		
		JButton btnAnimate = new JButton("Animate");
		btnAnimate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				kanwa.animate();
			}
		});
		
		btnAnimate.setBounds(100, 239, 80, 23);
		contentPane.add(btnAnimate);
		
		JSlider slider = new JSlider(JSlider.HORIZONTAL,1,10,3);
		slider.addChangeListener(new ChangeListener() {
	         public void stateChanged(ChangeEvent e) {
	            Figura.przes=(slider.getValue());
	         }
		});
	 		slider.setBounds(220, 239, 200, 23);
			contentPane.add(slider);
		addComponentListener(new ComponentAdapter() {
			public void componentResized(ComponentEvent e) {

		        kanwa.setBounds(10, 11, contentPane.getWidth()-12, contentPane.getHeight()-42);
		        contentPane.add(kanwa);
		        kanwa.initialize();
		    	btnAdd.setBounds(10, contentPane.getHeight()-22, 80, 23);	
		    	btnAnimate.setBounds(100, contentPane.getHeight()-22, 100, 23);
		    	slider.setBounds(220, contentPane.getHeight()-22, 200, 23);


		      }
		 });

		 }
	}


