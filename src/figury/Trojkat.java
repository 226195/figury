package figury;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.Polygon;

public class Trojkat extends Figura
{

	public Trojkat(Graphics2D buffer, int delay, int width, int height) 
	{
		super(buffer, delay, width, height);
		shape = new Polygon(new int[] {0,10,5}, new int[] {0,0,9}, 3);
		aft = new AffineTransform();
		area = new Area(shape);
	}
}